# HedgeDocs Role

> The purpose of this role is to setup HedgeDocs pad service.

## Dependencies

[Official documentation](https://github.com/hedgedoc/hedgedoc/blob/master/docs/setup/manual-setup.md)

This service has been designed for Debian GNU/Linux 10.

1. It will install dependencies required to run the service. You just have to fill variables described just below into your configuration file:

- Node.js 10.13+
- PostgreSQL
- NPM
- Yarn

2. Please install postgresql module as well: https://docs.ansible.com/ansible/latest/collections/community/postgresql

3. You will have to manage SSL configuration on your own by using Let's Encrypt or ZeroSSL. If you don't know the path to save your keys, please have a look to the nginx configuration file.

## Variables

Please look at variables from this file `vars/main.yml`

## Example Playbook

    - hosts: hedgedocs-server
      roles:
         - { role: hedgedocs-pad-lqdn,
             hedgedoc_version: "1.7.0-rc2"
             service_hedgedocs_domain : "pad.test.lqdn.fr"
             hedgedoc_db_name : hedgedocs-user
             hedgedoc_db_user : hedgedocs
             hedgedoc_sql_password : "{{ vault_hedgedoc_sql_password }}"
             hedgedoc_session_secret : "{{ vault_hedgedoc_session_secret }}"
         }

## Webserver configuration 

You will need to configure a web server to run this role. You can use the [Ansible role for Nginx by Jeff Geerlingguy](https://github.com/geerlingguy/ansible-role-nginx) for that. 

## Licence

hedgedocs-lqdn
Copyright (C) 2021  nono

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Author Information
------------------

Written by nono <np@laquadrature.net> in 2021, for La Quadrature du Net ( lqdn.fr ).
